import * as React from 'react';
import {useEffect, useState} from 'react';
import {useKeycloak} from "@react-keycloak/web";
import {Redirect} from "react-router-dom";
import axios from "axios";
import {styled} from '@mui/material/styles';
import List from '@mui/material/List';
import {TextField} from "@material-ui/core";
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {Button} from "@mui/material";
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Checkbox from '@mui/material/Checkbox';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import Card from '@mui/material/Card';
import Test from "../components/Test";
import MuiAlert from '@mui/material/Alert';

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const CustomCard = styled(Card)`

  margin-left: auto;
  margin-right: auto;
  width: 400px;
  margin-top: 40px;
  height: 250px;
`;
export default function TestPage() {

    const divStyle = {
        padding: "20px"
    };

    const {keycloak} = useKeycloak()
    const [profile, setProfile] = useState(null);
    const [test, setTest] = useState([]);
    const [open, setOpen] = React.useState(false);
    const [open2, setOpen2] = React.useState(false);


    //const [username, setUsername] = useState('')
    const [value, setValue] = React.useState(null);
    const [date, setDate] = React.useState(null);
    const [result, setResult] = useState(false)
    const onChangeResult = (result) => {
        setResult(result);
    }
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
        setOpen2(false);
    };
    const action = (
        <React.Fragment>
            <Button color="secondary" size="small" onClick={handleClose}>
                Fermer
            </Button>
            <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={handleClose}
            >
                <CloseIcon fontSize="small"/>
            </IconButton>
        </React.Fragment>
    );

    useEffect(() => {
        keycloak.loadUserProfile()
            .then(response => {
                setProfile({
                    username: response.username,
                    firstname: response.firstName,
                    lastname: response.lastName,
                    email: response.email
                })
                fetchUserTest(response.username)
            })
    }, [])
    const fetchUserTest = (username) => {
        axios.get(`${process.env.REACT_APP_API_URL}/api/tests/${username}`)
            .then((res) => {
                    if (res.data.length !== 0) {
                        setTest(res.data)
                    }
                }
            )
    }
    const handleTest = () => {
        const data = {
            username: profile.username,
            test_date: date,
            result: result,
        }
        console.log("SUBMIT", profile.username, date, result)
        axios.post(`${process.env.REACT_APP_API_URL}/api/tests/doTest`, data, {
            headers: {
                "Access-Control-Allow-Origin": "*",
                'X-CSRFToken': 'csrftoken',
                'Content-Type': 'application/json'
            },
        }).then((response) => {
            console.log("Test created", response)
            setTest([...test, data])
            setOpen(true);

        }).catch((error) => {
            console.log("ERROR", error)
            setOpen2(true);

        })
    }


    return (
        <>
            {
                !keycloak?.authenticated && <Redirect to={"/"}/>
            }

            {
                keycloak?.authenticated && (
                    <>


                        <div style={{'marginRight': 'auto', 'marginLeft': 'auto', 'width': '120px'}}>
                            <h2>Vos tests</h2>
                        </div>
                        <div>

                            {
                                test.length !== 0 &&
                                test.map((test, index) => {
                                    return (
                                        <div style={{'width': '400px', 'margin-left': 'auto', 'margin-right': 'auto'}}>
                                            <Accordion style={{'margin-bottom': '20px'}}>
                                                <AccordionSummary
                                                    expandIcon={<ExpandMoreIcon/>}
                                                >
                                                    <Typography>Test {index}</Typography>
                                                </AccordionSummary>
                                                <AccordionDetails>
                                                    <List>
                                                        <Test test={test} numero={index}/>

                                                    </List>
                                                </AccordionDetails>
                                            </Accordion>

                                        </div>
                                    )
                                })
                            }
                        </div>
                        <CustomCard sx={{minWidth: 275, maxWidth: 310}}>

                            <div style={{'textAlign': 'center'}}>
                                <h4> Ajouter un nouveau test </h4>
                            </div>

                            <div style={{'textAlign': 'center'}}>


                                <LocalizationProvider dateAdapter={AdapterDateFns}>
                                    <DatePicker
                                        label="Date du test"
                                        //  onChange={(event) =>
                                        //    setDate(event.target.value)}
                                        value={value}
                                        onChange={(newValue) => {
                                            setValue(newValue);
                                            setDate(newValue)
                                        }}
                                        renderInput={(params) => <TextField {...params} />}
                                    />
                                </LocalizationProvider>

                            </div>

                            <div style={{'textAlign': 'center'}}>


                                <br>
                                </br>
                                Cocher si le test est positif
                                <Checkbox label="test"

                                          onChange={(event) => setResult(!result)}
                                          value={result}
                                          defaultChecked={result}> Le test est-il positif ?
                                </Checkbox>
                            </div>
                            <div style={{'textAlign': 'center'}}>
                                <Button
                                    onClick={handleTest} variant="outlined"> Entrer
                                </Button>
                            </div>

                        </CustomCard>

                        <Snackbar
                            open={open}
                            autoHideDuration={6000}
                            onClose={handleClose}
                            action={action}
                        >
                            <Alert onClose={handleClose} severity="success" sx={{width: '100%'}}>
                                Test ajouté avec succès
                            </Alert>
                        </Snackbar>
                        <Snackbar
                            open={open2}
                            autoHideDuration={6000}
                            onClose={handleClose}
                            action={action}
                        >
                            <Alert onClose={handleClose} severity="error" sx={{width: '100%'}}>
                                Erreur, réessayez ultérieurement
                            </Alert>
                        </Snackbar>
                    </>
                )
            }
        </>
    )
}

