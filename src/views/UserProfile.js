import * as React from 'react'
import {CircularProgress, makeStyles, TextField} from "@material-ui/core";
import {useEffect, useState} from "react";
import {useKeycloak} from "@react-keycloak/web";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import {Button} from "@mui/material";
import axios from "axios";
import MuiAlert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});


const useStyles = makeStyles({
    divStyle: {
        padding: "20px",
    }
});


  
const UserProfile = () => {

    const {keycloak} = useKeycloak()
    const [open, setOpen] = React.useState(false);
    const [open2, setOpen2] = React.useState(false);
    const [profile, setProfile] = useState(null);
    const classes = useStyles();

    useEffect(() => {
        keycloak.loadUserProfile()
            .then(response => {
                setProfile({
                    username: response.username,
                    firstname: response.firstName,
                    lastname: response.lastName,
                    email: response.email
                })
            })
    }, [])
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
        setOpen2(false);
      };
      const action = (
        <React.Fragment>
          <Button color="secondary" size="small" onClick={handleClose}>
            Fermer
          </Button>
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={handleClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        </React.Fragment>
      );
    const handleUpdateUserInformation = (parameter, value) => {
        setProfile(profile => {
            profile[parameter] = value;
            return profile;
        })
    }

    const handleUpdateUser = () => {
        axios.post(`${process.env.REACT_APP_API_URL}/api/users/updateUser`, {
            username: profile.username,
            lastName: profile.lastname,
            firstName: profile.firstname,
            email: profile.email
        }).then(response => {
            console.log("RESPONSE", response)
            setOpen(true)
        }).catch((error) => {
            console.log("ERROR", error)
            setOpen2(true);
          
        })
    }


    return (
        <div className={classes.divStyle}>
            {profile === null && <CircularProgress/>}
            {profile !== null && (
                <>
                
                 <Card style={{'display': 'flex', 'flex-direction': 'column', 'maxWidth':'400px', 'minWidth':'400px', 'margin-left': 'auto','margin-right': 'auto','width':'400px', 'height':'390px', 'marginTop':'40px'}}>
                
                     <CardContent>
                     <div style={{'textAlign':'center'}}>
                    <h1> Mon Profil </h1>
                   
                    <TextField style={{'width':'250px'}}
                        label="prénom"
                        defaultValue={profile.firstname}
                        type="text"
                        onChange={(event) => {
                            handleUpdateUserInformation("firstname", event.target.value)
                        }}
                    />
                    <TextField style={{'marginTop':'20px','width':'250px'}}
                        label="nom de famille"
                        defaultValue={profile.lastname}
                        type="text"
                        onChange={(event) => {
                            handleUpdateUserInformation("lastname", event.target.value)
                        }}
                    />
                    <TextField style={{'marginTop':'20px','width':'250px'}}
                        label="Email"
                        defaultValue={profile.email}
                        type="email"
                        onChange={(event) => {
                            handleUpdateUserInformation("email", event.target.value)
                        }}
                    />
                    </div>
                    </CardContent>
                 
                    <div style={{'textAlign':'center'}}>
                    <Button variant="outlined" 
                    style={{'marginTop':'10px'}}
                        onClick={handleUpdateUser}>
                       Mettre à jour
                    </Button>
                    </div>
                    </Card>
                   
                </>
            )}
               <Snackbar
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
            action={action}
          >
           <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
           Profil mis à jour avec succès
        </Alert>
        </Snackbar>
           <Snackbar
            open={open2}
            autoHideDuration={6000}
            onClose={handleClose}
            action={action}
          >
            <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
          Erreur, réessayez ultérieurement
        </Alert>
        </Snackbar>
        </div>
    )
}

export default UserProfile;

