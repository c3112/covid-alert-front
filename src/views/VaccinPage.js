/*
import * as React from 'react'
import {useEffect, useState} from 'react'
import axios from "axios";

import {Button} from "@mui/material";
import {useKeycloak} from "@react-keycloak/web";
import {Accordion, AccordionDetails, makeStyles, TextField} from "@material-ui/core";
import Vaccin from "../components/Vaccin";
import Card from '@mui/material/Card';
import {styled} from '@mui/material/styles';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import MuiAlert from '@mui/material/Alert';
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const CustomCard = styled(Card)`

  margin-left: auto;
  margin-right: auto;
  width: 400px;
  margin-top: 40px;
  height: 270px;
`;
const useStyles = makeStyles({
    divStyle: {
        marginBottom: "20px",
    },
});

const VaccinPage = (...props) => {

    const classes = useStyles();
    const {keycloak} = useKeycloak()
    const [open, setOpen] = React.useState(false);
    const [open2, setOpen2] = React.useState(false);

    const [profile, setProfile] = useState(null)
    const [vaccin, setVaccin] = useState([])

    const [type, setType] = useState(null)
    const [date, setDate] = useState(null)
    const [complet, setComplet] = useState(false)
    const [updloadImage, setUploadImage] = useState(null)

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
        setOpen2(false);
    };
    const action = (
        <React.Fragment>
            <Button color="secondary" size="small" onClick={handleClose}>
                Fermer
            </Button>
            <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={handleClose}
            >
                <CloseIcon fontSize="small"/>
            </IconButton>
        </React.Fragment>
    );

    useEffect(() => {
        if (keycloak?.authenticated) {
            keycloak.loadUserProfile()
                .then(response => {
                    setProfile({
                        username: response.username,
                        firstname: response.firstName,
                        lastname: response.lastName,
                        email: response.email
                    })
                    fetchUserVaccin(response.username)
                })
        }
    }, [])


    const fetchUserVaccin = (username) => {
        axios.get(`${process.env.REACT_APP_API_URL}/api/vaccins/${username}`)
            .then((res) => {
                    if (res.data.length !== 0) {
                        setVaccin(res.data)
                    }
                }
            )
    }

    const onImageChange = (event) => {
        console.log(event.target.files[0])
        if (event.target.files && event.target.files[0]) {
            setUploadImage(event.target.files[0]);
        }
    }

    const postUserVaccin = () => {
        const formData = new FormData();

        formData.append("username", profile.username)
        formData.append("vaccin_date", date)
        formData.append("vaccin_type", type)
        formData.append("complet", complet)
        formData.append("image", updloadImage)


        // {
        //     username: profile.username,
        //     vaccin_date: date,
        //     vaccin_type: type,
        //     complet: complet,
        //     image: updloadImage
        // }
        console.log("FILE UPLOAD", updloadImage)

        axios.post(`${process.env.REACT_APP_API_URL}/api/vaccins/doVaccin`, formData, {
            headers: {
                "Content-Type": "multipart/form-data",
                // "Access-Control-Allow-Origin": "*",
                // 'X-CSRFToken': 'csrftoken',
                'Authorization': `Bearer ${keycloak.token}`
            }
        }).then((response) => {
            //setVaccin([...vaccin, data])

            console.log("RESPONSE UPLOAD IMAGE", response)
            setOpen(true)

            setDate(null)
            setComplet(null)
            setUploadImage(null)

        }).catch((error) => {
            console.log("ERROR", error)
            setOpen2(true);

        })
    }

    return (


        <div style={{'display': 'flex', 'flex-direction': 'column'}}>
            <div style={{'marginRight': 'auto', 'marginLeft': 'auto', 'width': '140px'}}>
                <h2>Vos Vaccins</h2>
            </div>


            {
                vaccin.map((vaccin_data, index) => {
                    return (
                        <div style={{'width': '600px', 'margin-left': 'auto', 'margin-right': 'auto'}}>
                            <Accordion style={{'margin-bottom': '20px'}}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon/>}
                                >
                                    <Typography>Vaccin {index}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <List>
                                        <Vaccin vaccin={vaccin_data} numero={index}/>
                                    </List>
                                </AccordionDetails>
                            </Accordion>
                        </div>
                    )
                })
            }

            {vaccin.length < 10 && (
                <div style={{'marginRight': 'auto', 'marginLeft': 'auto', 'width': '310px'}}>
                    <CustomCard sx={{minWidth: 275, maxWidth: 310}}>
                        <div style={{'textAlign': 'center'}}>
                            <h4> Ajouter un nouveau vaccin </h4>
                        </div>
                        <div style={{'textAlign': 'center'}}>


                            <TextField
                                type={"date"}
                                // label="Date du vaccin"
                                value={null}
                                onChange={(event) => {
                                    setDate(event.target.value)
                                }}/>
                        </div>


                        <div style={{'textAlign': 'center'}}>

                            <TextField
                                label="Type"
                                defaultValue={vaccin.vaccin_type}
                                type="text"
                                onChange={(event) => {
                                    setType(event.target.value)
                                }}/>
                        </div>

                        {/!*<div style={{'textAlign': 'center'}}>
                            <Checkbox
                                onChange={(event) => {
                                    setComplet(event.target.checked)
                                }}
                                checked={complet}> Vacciné
                            </Checkbox>
                        </div>*!/}

                        <input
                            type="file"
                            onChange={(event) => onImageChange(event)} className="filetype"
                        />

                        {/!*{updloadImage &&*!/}
                        {/!*    <img src={props.vaccin.imageupdloadImage} alt="preview image" style={{'maxWidth': '10em', "maxHeight": '10em'}}/>*!/}
                        {/!*}*!/}

                        <div style={{'textAlign': 'center'}}>
                            <Button
                                type={"submit"}
                                variant="outlined"
                                onClick={() => postUserVaccin()}>
                                Valider
                            </Button>
                        </div>
                    </CustomCard>

                </div>
            )}

            <Snackbar
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
                action={action}
            >
                <Alert onClose={handleClose} severity="success" sx={{width: '100%'}}>
                    Vaccin ajouté avec succès
                </Alert>
            </Snackbar>
            <Snackbar
                open={open2}
                autoHideDuration={6000}
                onClose={handleClose}
                action={action}
            >
                <Alert onClose={handleClose} severity="error" sx={{width: '100%'}}>
                    Erreur, réessayez ultérieurement
                </Alert>
            </Snackbar>
        </div>

    )
}

export default VaccinPage

*/

import * as React from 'react'
import axios from "axios";

import {Checkbox} from "@mui/material";
import {useEffect, useState} from "react";
import {useKeycloak} from "@react-keycloak/web";
import {FormGroup, makeStyles, TextField} from "@material-ui/core";
import {Button} from "@mui/material";
import Vaccin from "../components/Vaccin";
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import List from '@mui/material/List';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import {styled} from '@mui/material/styles';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import MuiAlert from '@mui/material/Alert';

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const CustomCard = styled(Card)`
  
  margin-left: auto;
  margin-right: auto;
  width: 400px;
  margin-top: 40px;
  height: 270px;
`;
const useStyles = makeStyles({
    divStyle: {
        marginBottom: "20px",
    },
});

const VaccinPage = (...props) => {

    const classes = useStyles();
    const {keycloak} = useKeycloak()
    const [open, setOpen] = React.useState(false);
    const [open2, setOpen2] = React.useState(false);

    const [profile, setProfile] = useState(null)
    const [vaccin, setVaccin] = useState([])

    const [type, setType] = useState(null)
    const [date, setDate] = useState(null)
    const [complet, setComplet] = useState(false)

    const [updloadImage, setUploadImage] = useState(null)

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
        setOpen2(false);
    };
    const action = (
        <React.Fragment>
            <Button color="secondary" size="small" onClick={handleClose}>
                Fermer
            </Button>
            <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={handleClose}
            >
                <CloseIcon fontSize="small" />
            </IconButton>
        </React.Fragment>
    );

    useEffect(() => {
        if (keycloak?.authenticated) {
            keycloak.loadUserProfile()
                .then(response => {
                    setProfile({
                        username: response.username,
                        firstname: response.firstName,
                        lastname: response.lastName,
                        email: response.email
                    })
                    fetchUserVaccin(response.username)
                })
        }
    }, [])


    const fetchUserVaccin = (username) => {
        axios.get(`${process.env.REACT_APP_API_URL}/api/vaccins/${username}`)
            .then((res) => {
                    if (res.data.length !== 0) {
                        setVaccin(res.data)
                    }
                }
            )
    }

    const onImageChange = (event) => {
        console.log(event.target.files[0])
        if (event.target.files && event.target.files[0]) {
            setUploadImage(event.target.files[0]);
        }
    }

    const postUserVaccin = () => {
        const formData = new FormData();

        formData.append("username", profile.username)
        formData.append("vaccin_date", date)
        formData.append("vaccin_type", type)
        formData.append("complet", complet)
        formData.append("image", updloadImage)


        const data ={
            username: profile.username,
            vaccin_date: date,
            vaccin_type: type,
            complet: complet,
            image: updloadImage
        }

        axios.post(`${process.env.REACT_APP_API_URL}/api/vaccins/doVaccin`, formData, {
            headers: {
                "Content-Type": "multipart/form-data",
                // "Access-Control-Allow-Origin": "*",
                // 'X-CSRFToken': 'csrftoken',
                'Authorization': `Bearer ${keycloak.token}`
            }
        }).then((response) => {
            setVaccin([...vaccin, data])

            console.log("RESPONSE UPLOAD IMAGE", response)
            setOpen(true)

            setDate(null)
            setComplet(null)
            setUploadImage(null)

        }).catch((error) => {
            console.log("ERROR", error)
            setOpen2(true);

        })
    }

    return (


        <div style={{'display': 'flex', 'flex-direction': 'column'}}>
            <div style={{'marginRight':'auto', 'marginLeft':'auto', 'width':'140px'}}>
                <h2>Vos Vaccins</h2>
            </div>


            {
                vaccin.map((vaccin, index) => {
                    return (
                        <div style={{'width':'600px','margin-left':'auto', 'margin-right':'auto'}}>
                            <Accordion style={{'margin-bottom':'20px'}}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                >
                                    <Typography>Vaccin {index}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <List>
                                        <Vaccin vaccin={vaccin} numero={index}/>

                                    </List>
                                </AccordionDetails>
                            </Accordion>
                        </div>
                    )
                })
            }

            {vaccin.length < 3 && (
                <div style={{'marginRight':'auto', 'marginLeft':'auto', 'width':'310px'}}>
                    <CustomCard sx={{minWidth: 275, maxWidth: 310}}>
                        <div style={{'textAlign':'center'}}>
                            <h4> Ajouter un nouveau vaccin </h4>
                        </div>
                        <div  style={{'textAlign':'center'}}>


                            <TextField
                                type={"date"}
                                // label="Date du vaccin"
                                value={null}
                                onChange={(event) => {
                                    setDate(event.target.value)
                                }}/>
                        </div>


                        <div  style={{'textAlign':'center'}}>

                            <TextField
                                label="Type"
                                defaultValue={vaccin.vaccin_type}
                                type="text"
                                onChange={(event) => {
                                    setType(event.target.value)
                                }}/>
                        </div>

                        <input
                            type="file"
                            onChange={(event) => onImageChange(event)} className="filetype"
                        />

                        <div  style={{'textAlign':'center'}}>
                            <Button
                                type={"submit"}
                                variant="outlined"
                                onClick={() => postUserVaccin()}>
                                Valider
                            </Button>
                        </div>
                    </CustomCard>

                </div>
            )}
            <Snackbar
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
                action={action}
            >
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Vaccin ajouté avec succès
                </Alert>
            </Snackbar>
            <Snackbar
                open={open2}
                autoHideDuration={6000}
                onClose={handleClose}
                action={action}
            >
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                    Erreur, réessayez ultérieurement
                </Alert>
            </Snackbar>
        </div>

    )
}

export default VaccinPage
