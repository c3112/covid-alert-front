import * as React from 'react'
import {useEffect} from 'react'
import {useKeycloak} from '@react-keycloak/web'
import {createStyles, makeStyles} from "@material-ui/core";
import axios from "axios";
import News from '../components/News.js';
import Alert from '@mui/material/Alert';

const useStyles = makeStyles((theme) =>
    createStyles({
        grid: {
            display: "flex",
        }
    }),
);


const Home = () => {

    const {keycloak} = useKeycloak()

    useEffect(() => {
        keycloak.loadUserProfile().then((userData) => {
            const username = userData.username;
            axios.get(`${process.env.REACT_APP_API_URL}/api/users/${userData.username}`)
                .then((getResponse) => {
                    if (getResponse.data.length === 0) {
                        addUserToDB(userData)
                    }
                })
            sendPosition(username)
        })
    }, [])

    const sendPosition = (username) => {
        navigator.geolocation.getCurrentPosition((position => {
            axios.post(`${process.env.REACT_APP_API_URL}/api/users/addLocations`, {
                username: username,
                longitude: position.coords.longitude,
                latitude: position.coords.latitude,
            }).then((response) => {
                console.log("RESPONSE POST GEOLOCATON", response)
            }).catch((error) => {
                console.log("ERROR", error)
            })
        }))
    }

    const addUserToDB = (profile) => {
        axios.post(`${process.env.REACT_APP_API_URL}/api/users/createUser`, {
            username: profile.username,
            firstName: profile.firstName,
            lastName: profile.lastName,
            email: profile.email,
            password: profile.password,
            enabled: true
        }, {
            headers: {
                "Access-Control-Allow-Origin": "*",
                'X-CSRFToken': 'csrftoken',
                'Content-Type': 'application/json'
            },
        }).then((response) => {
            console.log("REGISTRATION COMPLETE", response)
        }).catch((error) => {
            console.log("ERROR", error)
        })
    }

    return (
        <div>
            <h1>Bienvenue sur notre application Covid Alert !</h1>
           
            {/* { keycloak?.authenticated && (

                
                <Alert variant="outlined" severity="warning">
               Vous avez été en contact avec une personne positive à la covid19 — Vérifiez vos mails !
              </Alert>

                

            )
           
} */}
<News/>
            {
                !keycloak?.authenticated && (
                    <h4>Vous avez besoin de vous connecter pour continuer</h4>
                )

            }
           
        </div>
    )
}

export default Home
