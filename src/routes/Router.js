import * as React from 'react'
import {Route, Switch} from 'react-router-dom'
import {useKeycloak} from '@react-keycloak/web'

import Home from '../views/Home'
import PrivateRoute from './PrivateRoute'
import UserProfile from "../views/UserProfile";
import VaccinPage from "../views/VaccinPage";
import TestPage from "../views/TestPage";
import {useEffect, useState} from "react";

export const AppRouter = () => {

    const {keycloak} = useKeycloak()
    const [profile, setProfile] = useState(null);

    useEffect(() => {
        keycloak.loadUserProfile()
            .then(response => {
                setProfile({
                    username: response.username,
                    firstname: response.firstName,
                    lastname: response.lastName,
                    email: response.email
                })
            })
    }, [])
    
    return (
        <>
            {
                !keycloak && <div> Loading...</div>
            }
            {
                keycloak &&
                <Switch>
                    <PrivateRoute roles={['ROLE_USER']} path="/profile" component={UserProfile} exact/>
                    <PrivateRoute roles={['ROLE_USER']} path="/vaccin" component={VaccinPage}  exact/>
                    <PrivateRoute roles={['ROLE_USER']} path="/test" component={TestPage} exact/>
                    <Route exact path="/" component={Home}/>
                </Switch>
            }
        </>
    )
}
