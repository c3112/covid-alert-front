import * as React from 'react'

import {ReactKeycloakProvider} from '@react-keycloak/web'

import keycloak from './keycloak'
import {AppRouter} from './routes/Router'
 import AdapterDateFns from '@mui/lab/AdapterDateFns';
 import LocalizationProvider from '@mui/lab/LocalizationProvider';
import './index.css'
import NavBar from "./components/NavBar";
import {BrowserRouter as Router} from "react-router-dom";
require('dotenv').config()

const App = () => {

    const eventLogger = (event, error) => {
        console.log('EventLogger', event, error)
    }

    const tokenLogger = (tokens) => {
        console.log('TokenLogger', tokens)
    }

    return (
        <ReactKeycloakProvider
            authClient={keycloak}
            onEvent={eventLogger}
            onTokens={tokenLogger}>
            <Router>
                <NavBar/>
                <LocalizationProvider dateAdapter={AdapterDateFns}/>
                <AppRouter/>
            </Router>
        </ReactKeycloakProvider>
    )
}

export default App
