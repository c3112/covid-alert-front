import * as React from 'react'

export function userData(keycloak) {

    if (keycloak?.authenticated) {
        keycloak.loadUserProfile().then((data) => {
            return data
        })
    }

}
