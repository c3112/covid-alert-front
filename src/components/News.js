import * as React from 'react'
import axios from "axios";
import {useEffect, useState} from "react";

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';

//const date= "12-11-2021"
//var dc_tot = null
//const setData = useState(null)
const style = {
    width: '100%',
    maxWidth: 540,
    bgcolor: 'background.paper',
  };

const News = () =>  {
    
    const [news, setNews] = useState(null)   
    
    useEffect(() => {
        get_nb_cas() 
    }, [])

    const get_nb_cas = () => {
    
        axios.get(`${process.env.REACT_APP_API_URL}/api/news`, {
            headers: {
                'Content-Type': 'application/json'
            }}).then((res) => {
            if (res.data !== null) {
                console.log("ici", res.data[0].dc_tot)
            //  dc_tot=res.data[0].dc_tot
            //  const [data, setData] = useState(null)
                setNews({
                    dc_tot: res.data[0].dc_tot,
                    conf_j1: res.data[0].conf_j1,
                    pos: res.data[0].pos,
                    incid_rea: res.data[0].incid_rea,
                    incid_hosp: res.data[0].incid_hosp
                })
            }
        })
    }

    return (
        <>
        <div style={{ 'padding':'1em'}}>
          <h3> News sur le covid en France </h3> </div>
        {
            news !== null && (
                <List sx={style} component="nav" aria-label="mailbox folders">
                <ListItem >
                  <ListItemText primary="Cumul des décès:" /> {news.dc_tot} 
                </ListItem>
                <Divider />
                <ListItem  divider>
                  <ListItemText primary="Nombre de personnes déclarées positives(en 24h):" /> {news.pos} 
                </ListItem>
                <ListItem >
                  <ListItemText primary="Nombre de nouveaux patients hospitalisés(en 24h):" /> {news.incid_hosp} 
                </ListItem>
                <Divider light />
                <ListItem >
                  <ListItemText primary="Nombre de personnes en réanimation(en 24h):" /> {news.incid_rea} 
                </ListItem>
                <Divider  />
                <ListItem >
                  <ListItemText primary="Nombre de nouveaux cas confirmés(en 24h):" /> {news.conf_j1} 
                </ListItem>
                <Divider light />
                
              </List>

      
                
            )
        }
       
        </>
        
        
        
         
    )
    }
export default News;
