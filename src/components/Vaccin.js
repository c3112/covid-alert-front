import * as React from 'react'
import {Typography} from "@mui/material";
import Moment from 'moment';

const Vaccin = (props) => {

    return (
        <>
            <Typography>
                {`Dose ${props.numero} effectuée le ${Moment(props.vaccin.vaccin_date).format('DD MMM YYYY')} avec le vaccin  ${props.vaccin.vaccin_type} `}            </Typography>
            <img src={`data:image/jpeg;base64,${props.vaccin.image}`} alt={"image passe sanitaire"} style={{'maxWidth': '10em', "maxHeight": '10em'}}/>
        </>
    )
}

export default Vaccin
