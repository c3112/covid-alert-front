import * as React from 'react'
import {Typography} from "@mui/material";
import Moment from 'moment';

const Test = (props) => {
const res = props.test.result
if (res){
    return (
        <Typography>
            {`Test effectué le ${Moment(props.test.test_date).format('DD MMM YYYY')} - Résultat: Positif`}  
        </Typography>
    )
}
else {
    return (
        <Typography>
            {`Test effectué le ${Moment(props.test.test_date).format('DD MMM YYYY')} - Résultat: Negatif`}  
        </Typography>
    )
}
}

export default Test
