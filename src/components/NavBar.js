import * as React from 'react';
import { purple } from '@mui/material/colors';
import { styled } from '@mui/material/styles';
import {Link} from 'react-router-dom'
import {useKeycloak} from "@react-keycloak/web";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import {MenuItem} from "@material-ui/core";
import {Button} from "@mui/material";
import {IconButton} from "@mui/material";
import LogoutIcon from '@mui/icons-material/Logout';
import {useCallback} from "react";
import ConnectWithoutContactIcon from '@mui/icons-material/ConnectWithoutContact';

export default function NavBar() {

    const {keycloak} = useKeycloak()

    const ColorButton = styled(Button)(({ theme }) => ({
        color: theme.palette.getContrastText(purple[500]),
        //backgroundColor: purple[500],
        '&:hover': {
          backgroundColor: purple[700],
        },
      }));
    const login = useCallback(() => {
        keycloak.login();
    }, [keycloak]);

    const logout = useCallback(() => {
        keycloak.logout();
    }, [keycloak]);

    const register = useCallback(() => {
        keycloak.register();
    }, [keycloak]);

    return (
        <AppBar position="relative">
            <Toolbar>

                <Link to={"/"} style={{ textDecoration: 'none', color:"inherit" }}>   Covid-Alert</Link>
                {
                     !keycloak?.authenticated && (
                        <>
                            <MenuItem>
                                <ColorButton
                                    onClick={() => login()}>
                                   Login
                                </ColorButton>
                            </MenuItem>
                            <MenuItem>
                                <ColorButton
                                    onClick={() => register()}>
                                    Register
                                </ColorButton>
                            </MenuItem>
                        </>
                     )
                }
                {
                    keycloak?.authenticated && (
                        <>
                           
                            <MenuItem>
                                <Link to={"/profile"} style={{ textDecoration: 'none', color:"inherit"}}> Profil </Link>
                            </MenuItem>
                            <MenuItem>
                                <Link to={"/vaccin"} style={{ textDecoration: 'none', color:"inherit" }}> Vaccin </Link>
                            </MenuItem>
                            <MenuItem>
                                <Link to={"/test"} style={{ textDecoration: 'none', color:"inherit" }}> Test </Link>
                            </MenuItem>
                            <MenuItem>
                                <Button style={{'color':'black'}}
                                    onClick={() => logout()}>
                                    Logout
                                </Button>
                                <IconButton >
                                <LogoutIcon />
                                </IconButton>
                            </MenuItem>
                        </>
                    )
                }
            </Toolbar>
        </AppBar>
    );
}
